// JavaScript Document
$(document).ready(function () {
	$(".menu-icon-area").on('mouseover', function () {
		$(".menu-text").animate({
			opacity: 1,
			width: 100 + 'px'
		}, 300);
	});
	$(".menu-icon-area").on('mouseleave', function () {
		$(".menu-text").animate({

			width: 89 + 'px',
			opacity: 0
		}, 300);
	});
	// Menu btn Change case
	function setmenuContent() {
		var navlilent = $(".navigation ul li").length;
		var navliWidth = $(window).width() / navlilent;
		var navliHeight = $(window).height();
		var linkboxHeight = navliHeight - 150;
		var linkTopPosition = linkboxHeight / 2;
		$(".nav-link-box").css({
			"width": navliWidth + "px",
			'top': linkTopPosition + "px"
		});
	}

	function setmenuContentOut() {
		$(".nav-link-box").animate({
			width: 0 + "px"
		}, 300);
	}

	function showNavigation() {
		setmenuContent();
		$(".navigation").fadeIn();
		$(".navigation-animation").animate({
			width: 100 + '%',

		}, 800, function () {
			$(".navigation-animation").css({
				"left": "inherit",
				"right": "0px"
			});

		});
	}

	function hidNavigation() {

		$(".navigation-animation").animate({
			width: 0 + '%'
		}, 1000, function () {
			$(".navigation").fadeOut();
		});


	}

	$(".menu-icon-area").click(function () {

		$(this).toggleClass("change");
		$(".menu-text-showing").toggleClass("menu-text-hidden");
		$(".close-text-show").toggleClass("menu-text-show");

		//navitioncode
		if ($(".navigation").is(":visible")) {
			setmenuContentOut();
			hidNavigation();
		} else {
			showNavigation();
		}

	});
	$(".navigation-animation").on("mouseover", function () {
		$(this).find("h3").css({
			"top": " 0px"
		});
		$(this).find(".description-menu").css({
			"top": "10px"
		});
	});
	$(".navigation-animation").on("mouseleave", function () {
		$(this).find("h3").css({
			"top": " 60px"
		});
		$(this).find(".description-menu").css({
			"top": "180px"
		});
	});
	$(".full-screen").click(function () {
		$(this).toggleClass("restore-screen");
		$(".inner-left,.inner-half-l").toggleClass("inner-left-full");
	});


	var homeslideAuto;

	function slideShow() {

		$(".slide:first").fadeOut().next().fadeIn(1000).end().appendTo(".slideshow");
	}
	$(".next-slide").click(function () {
		clearInterval(homeslideAuto);
		slideShow();
		homeslideAuto = setInterval(function () {
			slideShow();
		}, 5000);

	});
	$(".pre-slide").click(function () {
		clearInterval(homeslideAuto);
		slideShow();
		homeslideAuto = setInterval(function () {
			slideShow();
		}, 5000);

	});

	homeslideAuto = setInterval(function () {
		slideShow();
	}, 5000);


	$(".slide").on("swipeleft", function () {
		clearInterval(homeslideAuto);
		slideShow();
		homeslideAuto = setInterval(function () {
			slideShow();
		}, 5000);
	});
	$(".slide").on("swiperight", function () {
		clearInterval(homeslideAuto);
		slideShow();
		homeslideAuto = setInterval(function () {
			slideShow();
		}, 5000);
	});
});